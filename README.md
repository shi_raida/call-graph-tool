# Call graph tool

## Table of contents

1. [Introduction](#1-introduction)

2. [Full Installation](#2-full-installation)

    2.0. [Prerequisites](#20-prerequisites)

    2.1. [Install Clang](#21-install-clang)
    
    2.2. [Install Linux Kernel](#22-install-linux-kernel)
    
    2.3. [Install Elisa's callgraph-tool](#23-install-elisas-callgraph-tool)
    
    2.4. [Install this call graph tool extension](#24-install-this-call-graph-tool-extension)
    
3. [Fast Installation](#3-fast-installation)
    
4. [Usage](#4-usage)

## 1. Introduction

I am using Ubuntu 18.04 LTS and I am working on `~/work` directory.

Moreover, python3 is necessary.

## 2. Full Installation

Use this installation method if you want to build the linux kernel with your own parameters and generate your own database.

Otherwise, go to [fast installation method](#3-fast-installation).

### 2.0. Prerequisites

Depending on your current installation, some packages may or may not be installed.

These packages can be installed with these command:

``` bash
foo@bar:~$ sudo apt install build-essential python3-pip git flex bison libssl-dev libelf-dev graphviz
foo@bar:~$ pip3 install cmake numpy
```

Then restart to make sure all of them are recognized properly (e.g. cmake is causing some issue)

### 2.1. Install Clang

According to Elisa's callgraph-tool documentation, it is easier to use Clang v10. However, the default version on Ubuntu 18.04 LTS is Clang v6 so we have to install it manually :

``` bash
foo@bar:~$ mkdir work && cd work
foo@bar:~/work$ git clone https://github.com/llvm/llvm-project.git && cd llvm-project
foo@bar:~/work/llvm-project$ mkdir build && cd build
foo@bar:~/work/llvm-project/build$ cmake -G "Unix Makefiles" -DLLVM_ENABLE_PROJECTS="clang" -DCMAKE_BUILD_TYPE=Release ../llvm
foo@bar:~/work/llvm-project/build$ make -jN (where N is up to the number of cores available on the computer)
foo@bar:~/work/llvm-project/build$ sudo make install/strip (needed this to make clang recognized by the environment)
```

More information here : https://github.com/elisa-tech/workgroups/blob/master/safety-architecture/tools/callgraph-tool/doc/clangsetup.md

### 2.2. Install Linux Kernel

Finally, we can install the linux kernel :

``` bash
foo@bar:~/work$ git clone https://github.com/torvalds/linux.git && cd linux
foo@bar:~/work/linux$ make CC=clang CCHOST=clang defconfig
foo@bar:~/work/linux$ make CC=clang CCHOST=clang -jN V=1 | tee buildlog.txt (where N is up to the number of cores available on the computer)
```

### 2.3. Install Elisa's callgraph-tool

An important thing is the Elisa's callgraph-tool scripts have to be in the same directory than the linux kernel files. Moreover, running `./callgraph-tool.py --build buildlog.txt` created ASM errors. I found a solution here : https://lists.elisa.tech/g/devel/message/758

Finally, we have to run the following commands :

``` bash
foo@bar:~/work$ cd ~/work && git clone https://github.com/elisa-tech/workgroups.git
foo@bar:~/work$ cp -r workgroups/safety-architecture/tools/callgraph-tool/* linux/ && cd linux
foo@bar:~/work/linux$ pip3 install -r requirements.txt
foo@bar:~/work/linux$ ./callgraph-tool.py --build buildlog.txt --build_log_format kernel_clang
```

### 2.4. Install this call graph tool extension

The scripts of this project have to be in the same directory than the linux kernel files.

``` bash
foo@bar:~/work/linux$ cd ~/work && git clone https://gitlab.com/shi_raida/call-graph-tool.git && cd call-graph-tool
foo@bar:~/work/call-graph-tool$ pip3 install -r requierements.txt && cd ..
foo@bar:~/work$ cp call-graph-tool/create-graph.py linux && cd linux
foo@bar:~/work/linux$ chmod +x create-graph.py
``` 

## 3. Fast Installation

Use this installation method if you don't want to build linux kernel with your own parameters and you want to use the database that provided you by call-graph-tool git repository.

Moreover, you will have the call graph without return type or parameters information with this method.

Otherwise, go to [full installation method](#2-full-installation).

``` bash
foo@bar:~/work git clone https://gitlab.com/shi_raida/call-graph-tool.git && cd call-graph-tool
foo@bar:~/work/call-graph-tool$ pip3 install -r requierements.txt
``` 

## 4. Usage

If you used [full installation method](#2-full-installation), you have to go on `~/work/linux` directory.
Otherwise, if you used [fast installation method](#3-fast-installation), you have to go on `~/work/call-graph-tool` directory.

To generate the graphviz graph of the `chksum_init` function you can run :

``` bash
./create-graph.py --function=chksum_init --view
```

The `--function` parameter allow you to specify the function and the `--view` parameter is for generate the graphviz render.

The graphviz outputs are stored under `output/{--function}` by default and can be changed with the `--output` option.

You can generate the graph of a whole file with the `--file` option. In this case, the outputs are stored under `output/{--file}` :

``` bash
./create-graph.py --file=crypto/crc32c_generic.c --view
```

By default the `call_graph.pickle` database (generated by Elisa's callgraph-tool) is used to generate the graph, but you can change it with the `--db` option.
