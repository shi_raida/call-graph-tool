#!/usr/bin/env python3

import argparse
import json
import os
import pickle

from clang.cindex import *
from graphviz import Digraph

import llvm_parse

macros = "__init __initdata __initconst __exit __exitdata __exitconst __exit_call __ref __refdata __refconst __weak " \
         "__meminit __meminitdata __meminitconst __memexit __memexitdata __memexitconst __nosavedata __pure " \
         "__attribute_const__".split(' ')
reserved = "auto _Bool break case char _Complex const continue default do double else enum extern float for goto if " \
           "_Imaginary inline int long register restrict return short signed sizeof static struct switch typedef " \
           "union unsigned void volatile while"

callgraph = {}
declared_functions = []  # Prevent infinite recursion
db = {}
unknown_functions = []  # (name, caller)
verbose = False


class Function:
    """
    Represents a function the call graph.
    """

    def __init__(self, function, declaration, depth):
        """
        :param function: Function stored in the database
        :param declaration: True if this object is the declaration of the function
                            False if it is only a call to this function
        :param depth: Current depth of the call graph
        :return: None
        """
        self.depth = depth
        self.name = function.name
        self.source_file = function.source_file
        if len(function.line_numbers) > 0:
            self.line = int(function.line_numbers[0])
        else:
            self.line = -1
            if verbose:
                if self.source_file != '':
                    print("\033[93mNo lines\033[0m : [{}] {}".format(self.source_file, self.name))
                else:
                    print("\033[93mNo source file\033[0m : {}".format(self.name))
        self.declaration = declaration
        if declaration:
            if self.name not in declared_functions:
                declared_functions.append(self.name)
            self.type = None
            self.global_var = []
            self.parse()
            if self.type is None:
                self.label = "[{}:{}]\n{}".format(self.source_file, self.line, self.name)
            else:
                self.label = "[{}:{}]\n{}\n{}\nGlobal variables:\n[{}]".format(self.source_file, self.line, self.name,
                                                                               self.type, ", ".join(self.global_var))
        else:
            self.label = self.name

    def __eq__(self, other):
        return self.name == other.name

    def __ne__(self, other):
        return self.name != other.name

    def __hash__(self):
        return hash(self.name)

    def parse(self):
        """
        Parse the source file to find the return type, parameters of the function, and global variables.

        :return: None
        """
        global unknown_functions

        not_global = []

        def find(_node):
            """
            Find the function node in the clang hierarchy.

            :param _node: Parent node
            :return: The function node
            """
            if _node.spelling == self.name and _node.is_definition():
                return _node
            else:
                for child in _node.get_children():
                    if find(child) is not None:
                        return child
                return None

        def find_name(_line):
            """
            Find the function names in the line.
            FIXME: Filter casts and macro definitions.

            :param _line: Line to parse
            :return: Function names array
            """
            if is_comment(_line):
                return []

            result = []
            if _line.find('(') != -1:
                if _line[_line.find('(') - 1] != ' ':
                    tmp = _line[:_line.find('(')]
                    if tmp.rfind(' ') != -1:
                        result.append(tmp[tmp.rfind(' ') + 1:])
                    else:
                        result.append(tmp)
                    if len(result[-1]) > 0:
                        if result[-1][0] in ['&', '~']:
                            result[-1] = result[-1][1:]
                result += find_name(_line[_line.find('(') + 1:])
            return [r for r in result if r not in reserved]  # Filter reserved words

        def find_text():
            """
            Find the function text.

            :return: The function text
            """
            txt = ''
            c = 0

            with open(self.source_file, 'r') as _file:
                for (i, _line) in enumerate(_file):
                    if i >= self.line - 1:
                        txt += _line
                    if _line.find('{') != -1 and i >= self.line - 1:
                        c += 1
                    if _line.find('}') != -1 and i >= self.line - 1:
                        c -= 1
                        if c == 0:
                            break
            return txt

        def is_comment(_line):
            """
            :param _line: Line to analyse.
            :return: Whether the line is a commentary.
            """
            trim = _line.strip()
            if len(trim) > 0:
                if trim[0] == '*':
                    return True
            if len(trim) > 1:
                if trim[0:2] in ['/*', '//']:
                    return True
            return False

        def get_globals(_node):
            """
            Fill the global_var variable by parsing the function.
            FIXME: Not find all global variables.

            :param _node: Function node
            :return: None
            """
            for child in _node.get_children():
                get_globals(child)

            if _node.kind == CursorKind.DECL_REF_EXPR and _node.spelling not in not_global + self.global_var:
                self.global_var.append(_node.spelling)
            elif _node.kind in (CursorKind.VAR_DECL, CursorKind.CALL_EXPR, CursorKind.PARM_DECL):
                not_global.append(_node.spelling)
                if _node.spelling in self.global_var:
                    self.global_var.remove(_node.spelling)

        def rm_macros(txt):
            """
            Remove macros from the text (ie. __init, __exit, etc).

            :param txt: Text where macros have to be removed
            :return: Text without macros
            """
            for macro in macros:
                txt = txt.replace(macro, '')
            return txt

        index = Index.create()
        if os.path.isfile(self.source_file):
            # Use a another file to parse because there was a parsing translation unit error with the
            # ./include/crypto/hash.h header file
            fun_txt = rm_macros(find_text())
            os.makedirs("tmp/", exist_ok=True)
            source_file = "tmp/nodeIsNone.c"
            with open(source_file, 'w') as file:
                file.write(fun_txt)

            tu = index.parse(source_file, options=TranslationUnit.PARSE_DETAILED_PROCESSING_RECORD)
            node = find(tu.cursor)

            if node is None:
                if verbose:
                    print("\033[93mNode is None\033[0m : [{}:{}] {}".format(self.source_file, self.line, self.name))
            else:
                self.type = node.type.spelling
                get_globals(node)

                unvisited = [i - 1 for i in range(node.extent.start.line + 1, node.extent.end.line + 1)]
                if len(unvisited) != 0:
                    with open(source_file, 'r') as f:
                        for (i, line) in enumerate(f):
                            line = line.replace('\n', '').replace('\t', '')
                            if i in unvisited and self.depth != 0:
                                if verbose:
                                    print("\t\033[93mUnvisited\033[0m : %s" % line)
                                names = find_name(line)
                                for name in names:
                                    create_graph(name, self.depth - 1, False)
                                    if (name, self) not in unknown_functions:
                                        unknown_functions.append((name, self))
        elif verbose:
            print("\033[93mNot a file\033[0m : [{}] {}".format(self.source_file, self.name))

    def to_json(self, called_functions):
        """
        Exports the this function in a JSON format.
        TODO: Export 'static', 'extern', etc attributes

        :param called_functions: List of the functions called by this object.
        :return: This function in a JSON format.
        """
        def get_inputs():
            """
            :return: The inputs of this function.
            """
            if self.type is None:
                return None
            return self.type[self.type.find('(')+1:self.type.rfind(')')].split(',')

        def get_output():
            """
            :return: The output of this function.
            """
            if self.type is None:
                return None
            return self.type[:self.type.find('(')]

        if self.declaration:
            return {"name": self.name,
                    "source_file": None if self.source_file == '' else self.source_file,
                    "line": None if self.line == -1 else self.line,
                    "inputs": get_inputs(),
                    "output": get_output(),
                    "global_variables": None if self.global_var == [] else self.global_var,
                    "called_functions": None if called_functions == [] else [f.name for f in called_functions]}
        else:
            return {"name": self.name,
                    "source_file": None if self.source_file == '' else self.source_file,
                    "line": None if self.line == -1 else self.line,
                    "inputs": None,
                    "output": None,
                    "global_variables": None,
                    "called_functions": None if called_functions == [] else [f.name for f in called_functions]}


def create_graph(function, depth, first_call):
    """
    Add the call graph of the function in the global callgraph dictionary.

    :param function: Function we want the call graph
    :param depth: Current depth of the call graph
    :param first_call: True if it the first call of the function
    :return: None
    """
    global callgraph
    global unknown_functions

    if function in [f.name for f in callgraph.keys()] or depth == -1 or function in declared_functions:
        return

    found = False
    for fun in db:
        if fun.name == function:
            found = True
            print("{} -> {}".format(fun.name, [f.name for f in db[fun]]))
            if depth == 0:
                callgraph[Function(fun, True, depth)] = []
            else:
                callgraph[Function(fun, True, depth)] = [Function(f, False, depth) for f in db[fun]]
            for f in db[fun]:
                create_graph(f.name, depth - 1, False)
            break

    if not found and first_call:
        callgraph[Function(llvm_parse.Function(function), False, depth)] = []

    if first_call:
        for fun in unknown_functions:
            name, caller = fun[0], fun[1]
            f = None
            for key in callgraph.keys():
                if key.name == name:
                    f = key

            if f is None:
                f = Function(llvm_parse.Function(name), False, depth)
                callgraph[f] = []

            if caller in callgraph.keys():
                if f not in callgraph[caller]:
                    callgraph[caller].append(f)
            else:
                callgraph[caller] = [f]


def create_view(output):
    """
    Create the graphviz view.

    :param output: File name to save the graphviz
    :return: None
    """
    print("Create view...")
    nodes = []
    dot = Digraph(comment="Call graph")
    for node in callgraph.keys():
        if node.name not in nodes and node.declaration:
            dot.node(node.name, label=node.label)
            nodes.append(node.name)
        for called in callgraph[node]:
            if called.name not in nodes and called.declaration:
                dot.node(called.name, label=called.label)
                nodes.append(called.name)
            dot.edge(node.name, called.name)

    for node in callgraph.keys():
        if node.name not in nodes:
            dot.node(node.name, label=node.label, shape="box")
            nodes.append(node.name)

    print("Rendering...")
    dot.render(output, view=True, format="svg")


def create_json(output):
    """
    Create an JSON file which describe the call graph.

    :param output: File name to save the JSON file
    :return: None
    """
    json_callgraph = []
    for node in callgraph.keys():
        json_callgraph.append(node.to_json(callgraph[node]))
    with open("{}.json".format(output), 'w') as file:
        file.write(json.dumps(json_callgraph))


def load_db(_db):
    """
    Load the database.

    :param _db: File name where the database is stored
    :return: None
    """
    global db

    with open(_db, 'rb') as stream:
        db = pickle.load(stream)


def get_functions(filename):
    """
    Return a list the functions of a specific file.

    :param filename: Name of the file
    :return: List of the functions
    """
    functions = []
    for function in db:
        if function.source_file == filename:
            functions.append(function)
    return functions


def get_args():
    """
    Parse console arguments.

    :return: Parsed arguments
    """
    parser = argparse.ArgumentParser(description="Call graph tool extension")
    parser.add_argument("--db", default=os.path.dirname(os.path.realpath(__file__)) + "/call_graph.pickle",
                        help="Use alternative db file")
    parser.add_argument("--depth", "-d", help="Limit the depth of the call graph")
    parser.add_argument("--file", "-F", metavar="FILE", help="Get call graph of a file")
    parser.add_argument("--function", "-f", metavar="FUNCTION", help="Get call graph of a function")
    parser.add_argument("--output", "-o", metavar="FILE", help="File name for the outputs (JSON and graphviz). By "
                                                               "default : output/function or output/file")
    parser.add_argument("--verbose", "-v", action="store_true", help="Activate the verbose mode")
    parser.add_argument("--view", "-V", action="store_true", help="View with graphviz")
    return parser.parse_args()


def main():
    """
    Main function of the script.

    :return: None
    """
    global verbose

    args = get_args()
    if not args.function and not args.file:
        print("No function or file specify. Ending...")
        return
    if args.function and args.file:
        print("Function and file specify. Cannot handle both. Ending...")
        return

    if args.verbose:
        verbose = True

    load_db(args.db)
    if args.depth:
        depth = int(args.depth)
    else:
        depth = -2  # Infinite depth

    if args.function:
        create_graph(args.function, depth, True)
    else:  # file
        functions = get_functions(args.file)
        for function in functions:
            create_graph(function.name, depth, True)

    if args.output:
        output = args.output
    elif args.function:
        output = "output/{}".format(args.function)
    else:  # file
        output = "output/{}".format(args.file)

    if args.view:
        create_view(output)
    create_json(output)


if __name__ == '__main__':
    main()
